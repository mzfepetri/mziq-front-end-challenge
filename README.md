# Aplicação Desafio MZ FrontEnd

Desafio front-end para a vaga de desenvolvedor no MZiQ

## Rodando o Front-End

Para executar o projeto, instale as dependências.

### `npm install`

Esse projeto foi criado utilizando o `create-react-app`, portanto para rodar o projeto, basta executar o comando `npm start` em seu terminal favorito.

Como a aplicação roda na porta [http://localhost:3000](http://localhost:3000). Eu mudei o back-end para rodar na porta [http://localhost:3004](http://localhost:3004)

## Rodando o Back-End

Para executar o servidor da API, rode o comando `npm install`, dentro do diretório 'api', em seguida execute o seguinte comando `npx json-server db.json --port 3004`

## Tecnologias Utilizadas

- A aplicação foi desenvolvida em SCSS seguindo o padrão da metodologia BEM na escrita das classes;
- Utilizei functional components e hooks para a criação dos componentes;
- Tentei quebrar os componentes de forma modularizada para facilitar o desenvolvimento e o entendimento do código;
- Separei as requisições axios em um arquivo separado chamado `/api/tools.js`; 
## Funcionalidades Implementadas
- [x] Listagem de Tools;
- [X] Adicionar uma nova Tool;
- [X] Remover uma Tool;
- [ ] Busca de Tools;