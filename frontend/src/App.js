import React, { Component } from 'react'
import Header from './components/Header'
import Tools from './components/tools/Tools'
import Footer from './components/Footer'

class App extends Component {
  render () {
    return (
        <div className="app">
            <Header />
            <Tools />
            <Footer />
        </div>  
    )
  }
}

export default App
