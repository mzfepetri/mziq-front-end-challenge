import axios from 'axios'

export function getTools () {
    const apiUrl = `http://localhost:3004/tools`
    return axios.get(apiUrl)
    .then((result) => {
        return result.data
    })
}

export function createNewTool (newTool) {
    const apiUrl = `http://localhost:3004/tools`
    return axios.post(apiUrl, newTool)
    .then((result) => {
        return result.data
    })
}
export function deleteTool (id) {
    const apiUrl = `http://localhost:3004/tools/${id}`
    return axios.delete(apiUrl)
    .then((result) => {
        return result.data
    })
}
const apiTools = { getTools, createNewTool, deleteTool }
export default apiTools