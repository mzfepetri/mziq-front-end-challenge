import React from 'react'

const Footer = () => (
    <footer className="footer">
        <div className="row no-gutters justify-content-between align-items-center">
            <p>
                2021&copy; Copyright
            </p>
            <p>
                powered by Petri
            </p>
        </div>
    </footer>
)

export default Footer