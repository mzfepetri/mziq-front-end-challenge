import React from 'react'

const Header = () => (
    <header className="header">
        <div className="row no-gutters">
            <h1 className="header__logo">
                <img src="/img/logo.png" alt='MZiQ' />
            </h1>
        </div>
    </header>
)

export default Header