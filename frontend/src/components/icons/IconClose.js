import React from 'react'

const IconClose = ({ onClick }) => (
    <div className="modal__content__close" onClick={ onClick }>
        <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M39.25 20C39.25 30.6315 30.6315 39.25 20 39.25C9.36852 39.25 0.75 30.6315 0.75 20C0.75 9.36852 9.36852 0.75 20 0.75C30.6315 0.75 39.25 9.36852 39.25 20Z" fill="white" stroke="#1DA6FF" strokeWidth="1.5"></path>
            <path d="M13.8182 14.5455L25.4545 26.1818" stroke="#113355" strokeWidth="1.5" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"></path>
            <path d="M25.4545 14.5455L13.8182 26.1818" stroke="#113355" strokeWidth="1.5" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"></path>
        </svg>
    </div>
)

export default IconClose