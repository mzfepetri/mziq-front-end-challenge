import React from 'react'

const Button = ({ onClick, children, type }) => {
    const buttonClass = type === 'remove' ? 'tools__listItens__single__removeItem btn btn--sm btn--white btn--bgRed' : "btn btn--white btn--bgPrimaryColor"
    return (
        <button 
            onClick={ onClick }
            className={ buttonClass }
        >
            { children }
        </button>
    )
}


export default Button