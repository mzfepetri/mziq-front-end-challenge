import React, { useState } from 'react'
import IconClose from '../icons/IconClose'

const AddToolModal = ({ openned, onClose, onSave }) => {
    const [toolTitleValue, toolTitleChange] = useState('')
    const [toolLinkValue, toolLinkChange] = useState('')
    const [toolDescriptionValue, toolDescriptionChange] = useState('')
    const [toolTagValue, toolTagChange] = useState('')
    const closeModal = () => {
        onClose(false)
    }
    const showAddModal = {
        display: (openned) ? 'flex' : 'none'
    };
    const onSubmit = (e) => {
        e.preventDefault()
        console.log(toolTitleValue)
        onSave({
            title: toolTitleValue,
            link: toolLinkValue,
            description: toolDescriptionValue,
            tags: toolTagValue.split(' ')
        })
    }
    return (
        <div className="modal" style={ showAddModal }>
            <div className="modal__content">
                <h2>
                    + Add new tool
                </h2>
                <IconClose
                    onClick= { closeModal }
                />
                <form onSubmit={ onSubmit }>
                    <label htmlFor="toolName">
                        Tool Name
                        <input
                            type="text"
                            id="toolName"
                            value= { toolTitleValue }
                            onChange={ (event) => toolTitleChange(event.target.value) }
                        />
                    </label>
                    <label htmlFor="toolLink">
                        Tool Link
                        <input
                            type="text"
                            id="toolLink"
                            name="toolLink"
                            value= { toolLinkValue }
                            onChange={ (event) => toolLinkChange(event.target.value) }
                        />
                    </label>
                    <label htmlFor="toolDescription">
                        Tool description
                        <input
                            type="text"
                            id="toolDescription"
                            name="toolDescription"
                            value= { toolDescriptionValue }
                            onChange={ (event) => toolDescriptionChange(event.target.value) }
                        />
                    </label>
                    <label htmlFor="toolTags">
                        Tags
                        <input
                            type="text"
                            id="toolTags"
                            name="toolTags"
                            value= { toolTagValue }
                            onChange={ (event) => toolTagChange(event.target.value) }
                        />
                    </label>
                    <label className="row justify-content-end">
                        <input 
                            type="Submit"
                            className="btn btn--white btn--bgPrimaryColor mt-4"
                            value="Add tool"
                        />
                    </label>
                </form>
            </div>
        </div>
    )
}

export default AddToolModal