import React from 'react'
import IconClose from '../icons/IconClose'

const RemoveModal = ({ openned, onClose, itemToRemove, toolName, removeTool}) => {
    const closeModal = () => {
        onClose(false)
    }
    const showRemoveModal = {
        display: (openned) ? 'flex' : 'none'
    }
    return(
        <div className="modal" style={ showRemoveModal }>
            <div className="modal__content js-removeModal">
                <IconClose 
                    onClick= { closeModal }
            />
                <h2>
                    - Remove tool
                </h2>
                <p>
                    Are you sure you want to remove <span>{ itemToRemove.title }</span>?
                </p>
                <div className="row justify-content-end">
                    <button
                        className="btn btn--white btn--bgPrimaryColor"
                        onClick= { closeModal }
                    >   
                        Cancel
                    </button>
                    <button
                        className="btn ml-3 btn--white btn--bgRed"
                        onClick= { removeTool }
                    >
                        Yes, remove
                    </button>
                </div>
            </div>
        </div>
    )
}

export default RemoveModal