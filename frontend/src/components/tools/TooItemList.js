import React from 'react'
import Button from '../shared/Button'

const ToolsItemList = ({ item, onRemove }) => {
    return (
        <div 
            className="tools__listItens__single" 
        >
            <div className="row no-gutters justify-content-between align-items-center">
                <h2>
                    <a href={ item.link } target="_blank">
                        { item.title }
                    </a>
                </h2>

                <Button type='remove' onClick= {() => onRemove(item) }>
                    X<span>Remove</span>
                </Button>

            </div>
            <p>
                { item.description }
            </p>

            <div className="tools__listItens__single__tags row no-gutters js-target-activeTagsSearch">
                { item.tags && item.tags.map((tag, index) =>(<div className="tools__listItens__single__tags__tag" key={index}>{tag}</div>))}
            </div>

        </div>
    )
}

export default ToolsItemList