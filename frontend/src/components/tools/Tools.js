import React, { useEffect, useState } from 'react'
import { getTools, deleteTool, createNewTool } from '../../api/tools'
import ToolsActions from './ToolsActions'
import ToolsList from './ToolsList'
import AddToolModal from './AddToolModal'
import RemoveToolModal from './RemoveToolModal'

const Tools = () => {

    const [listItens, setListItens] = useState([])
    useEffect(() => {
        const loadToolsList = async() => {
            const toolsList = await getTools()
            setListItens(toolsList)
        }
        loadToolsList()
    })

    const loadToolsList = async() => {
        const toolsList = await getTools()
        setListItens(toolsList)
    }
    
    const removeTool = async() => {
        await deleteTool(toRemoveTool.id)
        loadToolsList()
        setRemoveToolModalState(false)
    }

    const saveTool = async(toolItem) => {
        await createNewTool(toolItem)
        loadToolsList()
        setAddToolModalState(false)
    }

    const [isAddToolModalOpen, setAddToolModalState] = useState(false)
    const openAddModal = () => setAddToolModalState(true)

    const [isRemoveToolModalOpen, setRemoveToolModalState] = useState(false)
    const [toRemoveTool, setRemoveTool] = useState({})
    const openRemoveModal = (item) => {
        setRemoveToolModalState(true)
        setRemoveTool(item)
    }

    return (
        <main className="main">
            <div className="tools">
                <ToolsActions openAddModal={ openAddModal } />
                {!!listItens.length &&  <ToolsList
                    listItens = { listItens }
                    onRemoveItem = { openRemoveModal } 
                />}
            </div>
            <AddToolModal
                openned= { isAddToolModalOpen }
                onClose= { setAddToolModalState }
                onSave= { saveTool }
            />
            <RemoveToolModal
                openned= { isRemoveToolModalOpen }  
                onClose= { setRemoveToolModalState }
                itemToRemove= { toRemoveTool }
                removeTool = { removeTool }
            />
        </main>
    )
}

export default Tools