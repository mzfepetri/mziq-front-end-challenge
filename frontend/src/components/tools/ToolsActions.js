import React from 'react'
// import Search from './ToolsSearch'
import Button from '../shared/Button'

const ToolActions = ({ openAddModal }) => {
    return(
        <div className="tools__actions">
            <div className="row no-gutters align-items-center justify-content-between">
                {/* <Search /> */}
                <Button onClick= { openAddModal } >
                    + Add New
                </Button>
            </div>
        </div>
    )
}

export default ToolActions