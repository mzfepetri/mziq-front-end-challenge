import React from 'react'
import ToolsItemList from './TooItemList'

const List = ({ listItens, onRemoveItem }) => (
    <div className="tools__listItens">
        { (listItens || []).map((item, key) =>(
            <ToolsItemList
                key= { key }
                item= { item }
                onRemove= { onRemoveItem }
            />
        ))}
    </div>
)

export default List