import React from 'react'

const Search = ({ isDisabled, handleSearch, tagsOnly, tagsChecked }) => (
    <form className="tools__actions__search">
        <input
            type="search"
            placeholder='search'
            disabled={ isDisabled }
            onKeyUp={ handleSearch }
        />
        <label>
            <input
                type="checkbox"
                onChange={ tagsOnly }
                checked={ tagsChecked }
            />
            search in tags only
        </label>
    </form>
)

// Search.propTypes = {
//     isDisabled: PropTypes.bool.isRequired,
//     handleSearch: PropTypes.func.isRequired
// }

export default Search